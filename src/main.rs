use anyhow::Result;
use chad_torrent::backend::{DelugeBackend, QBittorrentBackend, TorrentBackend};
use chad_torrent::torrent::{Options, Torrent};

#[tokio::main]
async fn main() -> Result<()> {
    main_qbittorrent().await?;
    main_deluge().await
}

async fn main_qbittorrent() -> Result<()> {
    let backend = QBittorrentBackend::new("http://localhost:8080", "admin", "adminadmin").await?;

    /*
    let new_torrent = backend.add_magnet("magnet:?xt=urn:btih:53a9a99871a7beab25fc1b9d83713d696613aa65&dn=archlinux-2021.08.01-x86_64.iso", Options {
        name: Some("Arch Linux".into()),
        label: Some("chad".into()),
        save_path: None,
    }).await?;

    println!("{:#?}", new_torrent);
    */

    let list = backend.list(Some("chad")).await?;
    println!("{:#?}", &list);
    //let torrent = backend.torrent(list.get(0).unwrap().clone()).await?;
    //println!("{:#?}", &torrent);

    Ok(())
}

async fn main_deluge() -> Result<()> {
    //let backend = DelugeBackend::new("http://localhost:8112/json", "deluge").await?;
    let backend = DelugeBackend::default().await?;
    let hosts = backend.list_hosts().await?;
    println!("{:#?}", &hosts);

    if let Some(host) = hosts.get(1) {
        backend.connect(&host.id).await?;
        println!("Connected!");

        /*
        let new_torrent = backend.add_magnet("magnet:?xt=urn:btih:53a9a99871a7beab25fc1b9d83713d696613aa65&dn=archlinux-2021.08.01-x86_64.iso", Options {
            name: Some("Arch Linux".into()),
            label: Some("chad".into()),
            save_path: None,
        }).await?;

        println!("{:#?}", new_torrent);
        */

        let list = backend.list(Some("chad")).await?;
        println!("{:#?}", &list);

        for torrent in list {
            println!("{:#?}", torrent);

            println!("{:#?}", backend.torrent(&torrent.id).await?);
        }
    }

    Ok(())
}
