use crate::torrent::State;
use crate::{Torrent, TorrentBackend};
use anyhow::anyhow;
use anyhow::Result;
use async_json_rpc::clients::http::Client;
use async_json_rpc::prelude::*;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub struct DelugeBackend {
    client: Client<hyper::Client<hyper::client::HttpConnector>>,
}

const TORRENT_KEYS: &[&'static str] = &[
    "name",
    "hash",
    "label",
    "eta",
    "progress",
    "download_payload_rate",
    "upload_payload_rate",
    "num_peers",
    "save_path",
    "total_size",
    "total_done",
    "state",
];

#[derive(Clone, Debug, serde::Deserialize)]
pub struct DelugeTorrent {
    name: String,
    hash: String,
    label: Option<String>,
    eta: i64,
    progress: f64,
    download_payload_rate: f64,
    upload_payload_rate: f64,
    num_peers: i64,
    save_path: String,
    total_size: i64,
    total_done: i64,
    state: String,
}

/*
#[derive(Clone, Debug, serde::Deserialize)]
pub struct DelugeTorrent {
    active_time: u64,
    all_time_download: u64,
    auto_managed: bool,
    comment: String,
    completed_time: u64,
    creator: String,
    distributed_copies: f64,
    download_location: String,
    download_payload_rate: f64,
    eta: u64,
    file_priorities: Vec<serde_json::Value>,
    finished_time: u64,
    hash: String,
    is_auto_managed: bool,
    is_finished: bool,
    is_seed: bool,
    last_seen_complete: u64,
    max_connections: f64,
    max_download_speed: f64,
    max_upload_slots: f64,
    max_upload_speed: f64,
    message: String,
    move_completed: bool,
    move_completed_path: String,
    move_on_completed: bool,
    move_on_completed_path: String,
    name: String,
    next_announce: u64,
    num_files: u64,
    num_peers: u64,
    num_pieces: u64,
    num_seeds: u64,
    orig_files: Vec<serde_json::Value>,
    owner: String,
    paused: bool,
    peers: Vec<serde_json::Value>,
    piece_length: u64,
    prioritize_first_last: bool,
    prioritize_first_last_pieces: bool,
    private: bool,
    progress: f64,
    queue: u64,
    ratio: f64,
    remove_at_ratio: bool,
    save_path: String,
    seed_mode: bool,
    seed_rank: u64,
    seeding_time: u64,
    seeds_peers_ratio: f64,
    sequential_download: bool,
    shared: bool,
    state: String,
    stop_at_ratio: bool,
    stop_ratio: f64,
    storage_mode: String,
    super_seeding: bool,
    time_added: f64,
    time_since_download: f64,
    time_since_transfer: f64,
    time_since_upload: f64,
    total_done: f64,
    total_payload_download: f64,
    total_payload_upload: f64,
    total_peers: f64,
    total_remaining: f64,
    total_seeds: f64,
    total_size: f64,
    total_uploaded: f64,
    total_wanted: f64,
    tracker: String,
    tracker_host: String,
    tracker_status: String,
    trackers: Vec<serde_json::Value>,
    upload_payload_rate: f64,
    label: Option<String>,
}
*/

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Host {
    pub id: String,
    pub host: String,
    pub port: u64,
    pub username: String,
}

impl Host {
    pub fn from_array(array: Vec<serde_json::Value>) -> Result<Self> {
        Ok(Self {
            id: array[0].as_str().ok_or(anyhow!("Invalid id"))?.into(),
            host: array[1].as_str().ok_or(anyhow!("Invalid host"))?.into(),
            port: array[2].as_u64().ok_or(anyhow!("Invalid port"))?.into(),
            username: array[3].as_str().ok_or(anyhow!("Invalid username"))?.into(),
        })
    }
}

impl Into<Torrent> for DelugeTorrent {
    fn into(self) -> Torrent {
        Torrent {
            name: self.name,
            id: self.hash.clone(),
            label: self.label,
            eta: self.eta,
            progress: self.progress / 100.,
            download_speed: self.download_payload_rate,
            upload_speed: self.upload_payload_rate,
            num_peers: self.num_peers,
            save_path: self.save_path,
            size: self.total_size,
            downloaded: self.total_done,
            state: match self.state.as_str() {
                /*
                Source: deluge/common.py
                TORRENT_STATE = [
                    'Allocating',
                    'Checking',
                    'Downloading',
                    'Seeding',
                    'Paused',
                    'Error',
                    'Queued',
                    'Moving',
                ]
                 */
                "Error" => State::Error,
                "Downloading" | "Seeding" | "Allocating" | "Moving" => State::Active,
                "Paused" => State::Paused,
                "Queued" => State::Queued,
                "Checking" => State::Checking,
                _ => State::Other,
            },
        }
    }
}

impl DelugeBackend {
    pub async fn new(web_address: &str, web_password: &str) -> Result<Self> {
        let mut client = Client::new(web_address.into(), None, None);
        let res = client
            .send(
                client
                    .build_request()
                    .method("auth.login")
                    .params(vec![web_password])
                    .finish()
                    .unwrap(),
            )
            .await?;

        if res.result().transpose()? != Some(true) {
            return Err(anyhow!("Web login failed"));
        } else {
            client.set_cookie(
                res.cookie
                    .ok_or(anyhow!("Failed to login: missing cookie"))?,
            )
        }

        Ok(Self { client })
    }

    pub async fn default() -> Result<Self> {
        Self::new("http://localhost:8112/json", "deluge").await
    }

    pub async fn call<T: serde::de::DeserializeOwned>(
        &self,
        method: &str,
        args: &[serde_json::Value],
    ) -> Result<Option<T>> {
        let req = self
            .client
            .build_request()
            .method(method)
            .params(args)
            .finish();

        let req = if let Ok(req) = req {
            req
        } else {
            return Err(anyhow!("Invalid request"));
        };

        let res = self.client.send(req).await?;

        if res.is_error() {
            return Err(anyhow!("Request failed: {:?}", res.error()));
        }

        Ok(res.result().transpose()?)
    }

    pub async fn add_host(
        &self,
        host: &str,
        port: usize,
        username: &str,
        password: &str,
    ) -> Result<String> {
        if let Some((true, id)) = self
            .call(
                "web.add_host",
                &[host.into(), port.into(), username.into(), password.into()],
            )
            .await?
        {
            Ok(id)
        } else {
            Err(anyhow!("Failed to add host"))
        }
    }

    pub async fn list_hosts(&self) -> Result<Vec<Host>> {
        let res: Option<Vec<_>> = self.call("web.get_hosts", &[]).await?;

        let hosts = res
            .ok_or(anyhow!("Failed to get hosts"))?
            .into_iter()
            .filter_map(|h| Host::from_array(h).ok())
            .collect();
        Ok(hosts)
    }

    pub async fn connect(&self, host_id: &str) -> Result<()> {
        let res = self
            .call::<Option<serde_json::Value>>("web.connect", &[host_id.into()])
            .await?;

        if res.is_some() {
            Ok(())
        } else {
            Err(anyhow!("Failed to connect to daemon"))
        }
    }
}

#[async_trait]
impl TorrentBackend for DelugeBackend {
    async fn list(&self, filter_label: Option<&str>) -> Result<Vec<Torrent>> {
        let mut map = serde_json::Map::new();
        if let Some(filter) = filter_label {
            map.insert("label".into(), filter.into());
        }

        let torrents = self
            .call::<HashMap<String, DelugeTorrent>>(
                "core.get_torrents_status",
                &[map.into(), TORRENT_KEYS.into()],
            )
            .await?
            .unwrap_or(HashMap::new())
            .into_iter()
            .map(|(_, v)| v.into())
            .collect();

        Ok(torrents)
    }

    async fn pause(&self, torrent_id: &str) -> Result<()> {
        self.call::<serde_json::Value>("core.pause_torrent", &[torrent_id.into()])
            .await
            .map(|_| ())
    }

    async fn resume(&self, torrent_id: &str) -> Result<()> {
        self.call::<serde_json::Value>("core.resume_torrent", &[torrent_id.into()])
            .await
            .map(|_| ())
    }

    async fn add_magnet(&self, magnet: &str, options: crate::torrent::Options) -> Result<String> {
        let mut map = serde_json::Map::new();

        if let Some(save_path) = options.save_path {
            map.insert("download_location".into(), save_path.into());
        }

        if let Some(name) = options.name {
            map.insert("name".into(), name.into());
        }

        if let Some(id) = self
            .call::<String>("core.add_torrent_magnet", &[magnet.into(), map.into()])
            .await?
        {
            if let Some(label) = options.label {
                self.call::<serde_json::Value>(
                    "label.set_torrent",
                    &[id.clone().into(), label.into()],
                )
                .await?;
            }
            Ok(id)
        } else {
            Err(anyhow!("Failed add magnet"))
        }
    }

    async fn remove_torrent(&self, torrent_id: &str, remove_data: bool) -> Result<()> {
        if let Some(_) = self
            .call::<serde_json::Value>(
                "core.remove_torrent",
                &[torrent_id.into(), remove_data.into()],
            )
            .await?
        {
            Ok(())
        } else {
            Err(anyhow!("Failed to remove torrent"))
        }
    }

    async fn torrent(&self, torrent_id: &str) -> Result<Torrent> {
        let result: Option<DelugeTorrent> = self
            .call(
                "core.get_torrent_status",
                &[torrent_id.into(), TORRENT_KEYS.into()],
            )
            .await?;

        if let Some(torrent) = result {
            Ok(torrent.into())
        } else {
            Err(anyhow!("Failed to get torrent status"))
        }
    }
}
